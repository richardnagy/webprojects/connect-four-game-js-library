# Run all static and dynamic tests
all::
	@$(MAKE) -s test
	@$(MAKE) -s lint

# Run dynamic tests using jest
test:: node_modules
	NODE_OPTIONS='--experimental-vm-modules' npx jest .

# Run static tests using jest
lint:: node_modules
	npx eslint ./src

fix:: node_modules
	npx eslint ./src --fix

# Remove all temporary files and packages
clean::
	@rm -rf \
		node_modules/ \
		yarn-error.log

# Install dependencies
install::
	yarn install --frozen-lockfile

# Select specified version of nodejs runtime
use:: .nvmrc
	nvm install
	nvm use


# Update modules
node_modules: yarn.lock
	@$(MAKE) -s install
