// Helper methods

export function for_all_states(game_state, func) {
    for (const row of game_state) {
        for (const cell of row) {
            func(cell)
        }
    }
}

export function deep_copy_matrix(matrix) {
    return matrix.map(function(row) {
        return row.slice()
    })
}

export function validate_dimensions(game_state) {
    if (game_state == null) {
        return false
    }
    
    try {
        if (game_state.length != 6) {
            return false
        }
        for (const row of game_state) {
            if (row.length != 7) {
                return false
            }
        }
    }
    catch (err) {
        return false
    }

    return true
}

export function validate_cell_contents(game_state) {
    let y = 0
    let r = 0
    for_all_states(game_state, (cell) => {
        if (cell == 'y') {
            y++
        }
        else if (cell == 'r') {
            r++
        }
        else if (cell == null) { null }
        else {
            return false
        }
    })
    return Math.abs(y - r) < 2
}

export function validate_cell_positions(game_state) {
    for (let i = 0; i < game_state[0].length; i++) {
        let column_state = false
        for (let j = 0; j < game_state.length; j++) {
            const col = game_state[j][i]
            if (col == null && column_state) {
                return false
            }
            if (col != null) {
                column_state = true
            }
        }
    }
    return true
}

export function insert_into_column(game_state, column, player) {
    for (let i = 5; i >= 0; i--) {
        const col = game_state[i][column]
        
        if (col == null) {
            game_state[i][column] = player
            return true
        }
    }

    return false
}

export function check_line_winner(a, b, c, d) {
    return ((a != null) && (a == b) && (a == c) && (a == d))
}

// Business functions

/**
 * Determines which player has the next step
 * @param {string[][]} game_state current table state matrix
 * @returns {string} `'y'` if yellow player or `'r'` if red player
 */
export function get_current_player(game_state) {
    let empty = 0
    for_all_states(game_state, (cell) => {
        if (cell == null) {
            empty++
        }
    })
    return empty % 2 == 0 ? 'y' : 'r'
}

/**
 * Checks if the game table is a valid table
 * This is done by checking dimensions, cell contents and cell positions
 * @param {string[][]} game_state current table state matrix
 * @returns {bool} `true` or `false` depending on if the validation has passed
 */
export function is_state_valid(game_state) {
    if (!validate_dimensions(game_state)) return false
    if (!validate_cell_contents(game_state)) return false
    if (!validate_cell_positions(game_state)) return false

    return true
}

/**
 * Advance a game table with the given step
 * @param {string[][]} game_state current table state matrix
 * @param {int} column index of the column for the player to insert into
 * @param {string} player `'y'` or `'r'`, color of the current player
 * @returns {string[][]} a new game state matrix object
 */
export function play(game_state, column, player) {
    if (column < 0 || column > 6) {
        throw 'Column index must be between 0 and 6'
    }

    let new_state = deep_copy_matrix(game_state)
    insert_into_column(new_state, column, player)
    return new_state
}

/**
 * Determine if the game has a winner
 * @param {*} game_state current table state matrix
 * @returns {bool} `true` if there is a winner, `false` if there is not
 */
export function has_winner(game_state) {
    const s = game_state
    // Check vertical
    for (let r = 0; r < 3; r++)
        for (let c = 0; c < 7; c++)
            if (check_line_winner(s[r][c], s[r+1][c], s[r+2][c], s[r+3][c]))
                return true

    // Check horizontal
    for (let r = 0; r < 6; r++)
        for (let c = 0; c < 4; c++)
            if (check_line_winner(s[r][c], s[r][c+1], s[r][c+2], s[r][c+3]))
                return true

    // Check down-right
    for (let r = 0; r < 3; r++)
        for (let c = 0; c < 4; c++)
            if (check_line_winner(s[r][c], s[r+1][c+1], s[r+2][c+2], s[r+3][c+3]))
                return true

    // Check down-left
    for (let r = 3; r < 6; r++)
        for (let c = 0; c < 4; c++)
            if (check_line_winner(s[r][c], s[r-1][c+1], s[r-2][c+2], s[r-3][c+3]))
                return true

    return false
}

// Default exports

export default {
    get_current_player,
    is_state_valid,
    play,
    has_winner
}