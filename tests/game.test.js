import { describe, test, expect } from '@jest/globals'

import game from '../src/game'

const sample_states = {
    first_round: [
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
    ],
    second_round: [
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        ['y', null, null, null, null, null, null],
    ],
    valid_half: [
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, "y", "r", null, null, null],
        [null, null, "r", "y", null, null, null],
        ["r", "y", "y", "y", "r", "r", "y"],
        ["r", "r", "y", "y", "r", "y", "r"],
    ],
    invalid_position: [
        [null, null, null, null, null, null, null],
        [null, "r", null, null, null, null, null],
        [null, null, "y", "r", null, null, null],
        [null, null, "r", "y", null, null, null],
        ["r", "y", "y", "y", "r", "r", "y"],
        ["r", "r", "y", "y", "r", "y", "r"],
    ],
    invalid_symbol: [
        ['x', null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
    ],
}

describe('Unit tests: get_current_player', () => {
    test('First turn is y', () => {
        expect( game.get_current_player(sample_states.first_round) ).toBe('y')
    })
    test('Second turn is r', () => {
        expect( game.get_current_player(sample_states.second_round) ).toBe('r')
    })
    test('Halfway state is y turn', () => {
        expect( game.get_current_player(sample_states.valid_half) ).toBe('y')
    })
})

describe('Unit tests: is_state_valid', () => {
    test('Empty table is valid', () => {
        expect( game.is_state_valid(sample_states.first_round) ).toBeTruthy()
    })
    test('Single step is valid', () => {
        expect( game.is_state_valid(sample_states.second_round) ).toBeTruthy()
    })
    test('Halfway state is valid', () => {
        expect( game.is_state_valid(sample_states.valid_half) ).toBeTruthy()
    })

    test('Null table is invalid', () => {
        expect( game.is_state_valid(null) ).toBeFalsy()
    })
    test('Smaller table is invalid', () => {
        expect( game.is_state_valid([[null,null],[null,null]]) ).toBeFalsy()
    })
    test('Invalid symbol is invalid', () => {
        expect( game.is_state_valid(sample_states.invalid_symbol) ).toBeFalsy()
    })
})

describe('Unit tests: play', () => {
    test('First play goes to bottom', () => {
        expect( game.play([
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
        ], 0, 'y')).toStrictEqual([
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            ['y', null, null, null, null, null, null],
        ])
    })

    test('Second play goes on top', () => {
        expect( game.play([
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, 'y', null, null, null, null, null],
        ], 1, 'r')).toStrictEqual([
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, 'r', null, null, null, null, null],
            [null, 'y', null, null, null, null, null],
        ])
    })

    test('Full column does not change', () => {
        expect( game.play([
            [null, null, null, null, 'r', null, null],
            [null, null, null, null, 'r', null, null],
            [null, null, null, null, 'r', null, null],
            [null, null, null, null, 'r', null, null],
            [null, null, null, null, 'r', null, null],
            [null, null, null, null, 'r', null, null],
        ], 4, 'r')).toStrictEqual([
            [null, null, null, null, 'r', null, null],
            [null, null, null, null, 'r', null, null],
            [null, null, null, null, 'r', null, null],
            [null, null, null, null, 'r', null, null],
            [null, null, null, null, 'r', null, null],
            [null, null, null, null, 'r', null, null],
        ])
    })

    test('Invalid column throws error', () => {
        try {
            game.play([
                [null, null, null, null, 'r', null, null],
                [null, null, null, null, 'r', null, null],
                [null, null, null, null, 'r', null, null],
                [null, null, null, null, 'r', null, null],
                [null, null, null, null, 'r', null, null],
                [null, null, null, null, 'r', null, null],
            ], -1, 'r')
            expect(false)
        }
        catch {
            expect(true)
        }

    })

})

describe('Unit tests: has_winner', () => {
    test('Empty table has no winner', () => {
        expect( game.has_winner(sample_states.first_round) ).toBeFalsy()
    })
    test('Half table has no winner', () => {
        expect( game.has_winner(sample_states.valid_half) ).toBeFalsy()
    })
    test('Horizontal win', () => {
        expect( game.has_winner([
            ['r', 'r', 'r', 'r', null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
        ]) ).toBeTruthy()
    })
    test('Vertical win', () => {
        expect( game.has_winner([
            [null, null, null, null, null, null, null],
            [null, 'y', null, null, null, null, null],
            [null, 'y', null, null, null, null, null],
            [null, 'y', null, null, null, null, null],
            [null, 'y', null, null, null, null, null],
            [null, null, null, null, null, null, null],
        ]) ).toBeTruthy()
    })
    test('Diagonal right win', () => {
        expect( game.has_winner([
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, 'r'],
            [null, null, null, null, null, 'r', null],
            [null, null, null, null, 'r', null, null],
            [null, null, null, 'r', null, null, null],
        ]) ).toBeTruthy()
    })
    test('Diagonal left win', () => {
        expect( game.has_winner([
            [null, null, 'y', null, null, null, null],
            [null, null, null, 'y', null, null, null],
            [null, null, null, null, 'y', null, null],
            [null, null, null, null, null, 'y', null],
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
        ]) ).toBeTruthy()
    })
    test('Half table win', () => {
        expect( game.has_winner([
            [null, null, null, null, null, null, null],
            [null, null, null, null, null, null, null],
            ['r', null, "r", "y", null, null, null],
            ['y', null, "y", "y", null, 'r', null],
            ["r", "y", "y", "y", "r", "r", "y"],
            ["y", "r", "y", "y", "r", "y", "r"],
        ]) ).toBeTruthy()
    })

})